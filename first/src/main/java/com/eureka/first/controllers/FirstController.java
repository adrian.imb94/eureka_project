package com.eureka.first.controllers;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * The type First controller.
 */
@RestController
public class FirstController {

    /**
     * The Rest template.
     */
    @Autowired
    RestTemplate restTemplate;


    /**
     * Test string.
     *
     * @return the string
     * @throws InterruptedException the interrupted exception
     */
    @GetMapping(value = "/")
    @HystrixCommand(fallbackMethod = "fallback_test", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public String test() throws InterruptedException {
        Thread.sleep(3000);
        return "Hystrix test.";
    }


    private String fallback_test() {
        return "Petición fallida por timeout.";
    }
}
