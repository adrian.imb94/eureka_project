package com.eureka.rabbitmq;


import com.eureka.rabbitmq.components.Receiver;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


/**
 * Rabbitmq.
 *
 * @author Adrián Moral
 * @version 1
 *
 *  | Para lanzar en docker RabbitMQ utilizaremos el siguiente comando:
 *  - docker run -d -p 15672:15672 -p 5672:5672 --name rabbitmq rabbitmq:3-management
 *
 */
@SpringBootApplication
public class RabbitmqApplication {

	/**
	 * The constant topicExchangeName.
	 */
	public static final String topicExchangeName = "spring-boot-exchange";

	/**
	 * The Queue name.
	 */
	static final String queueName = "spring-boot";

	/**
	 * Queue queue.
	 *
	 * @return the queue
	 */
	@Bean
	Queue queue() {
		return new Queue(queueName, false);
	}

	/**
	 * Exchange topic exchange.
	 *
	 * @return the topic exchange
	 */
	@Bean
	TopicExchange exchange() {
		return new TopicExchange(topicExchangeName);
	}

	/**
	 * Binding binding.
	 *
	 * @param queue    the queue
	 * @param exchange the exchange
	 * @return the binding
	 */
	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with("foo.bar.#");
	}

	/**
	 * Container simple message listener container.
	 *
	 * @param connectionFactory the connection factory
	 * @param listenerAdapter   the listener adapter
	 * @return the simple message listener container
	 */
	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
											 MessageListenerAdapter listenerAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(queueName);
		container.setMessageListener(listenerAdapter);
		return container;
	}

	/**
	 * Listener adapter message listener adapter.
	 *
	 * @param receiver the receiver
	 * @return the message listener adapter
	 */
	@Bean
	MessageListenerAdapter listenerAdapter(Receiver receiver) {
		return new MessageListenerAdapter(receiver, "receiveMessage");
	}

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 * @throws InterruptedException the interrupted exception
	 */
	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(RabbitmqApplication.class, args).close();
	}

}