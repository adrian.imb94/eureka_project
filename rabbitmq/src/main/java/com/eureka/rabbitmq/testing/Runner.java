package com.eureka.rabbitmq.testing;

import java.util.concurrent.TimeUnit;

import com.eureka.rabbitmq.RabbitmqApplication;
import com.eureka.rabbitmq.components.Receiver;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Runner implements CommandLineRunner {

    private final RabbitTemplate rabbitTemplate;
    private final Receiver receiver;

    public Runner(Receiver receiver, RabbitTemplate rabbitTemplate) {
        this.receiver = receiver;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Enviando mensaje...");
        rabbitTemplate.convertAndSend(RabbitmqApplication.topicExchangeName, "foo.bar.baz", "Buenos días!");
        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
    }

}