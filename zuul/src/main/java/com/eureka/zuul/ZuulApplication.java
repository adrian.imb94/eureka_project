package com.eureka.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;


/**
 * Zuul.
 *
 * @author Adrián Moral
 * @version 1
 *
 *  * Testear fácil el funcionamiento:
 *	- Para utilizar Zuul como proxy escribiremos la dirección + puerto + nombre de eureka y accederemos al microservicio.
 *	- http://localhost:8765/second-info-service/api/personas
 *
 */
@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
public class ZuulApplication {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ZuulApplication.class, args);
	}

}
