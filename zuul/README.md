# Proyecto Back-end (Adrián Moral)

_Este proyecto trata de emular un proyecto de ejemplo empleando varias de las tecnologías vistas durante la formación._

## Zuul ⚙️

_Utilizaremos Zuul como API Gateway en nuestra aplicación, delegando en esta tecnología para la gestión de rutas._


_GIT_

Para introducir cambios en Zuul utilizaremos ramas de **feature**.


### Pre-requisitos 📋

_Para poder probar esta Gateway con el código establecido será necesario también incorporar el servidor de Eureka, o removerlo de las dependencias._

_Necesitaremos tambien trabajar con [Maven](https://maven.apache.org/)._

_También será necesario tener Java o algún IDE que nos lo proporcione automáticamente._
_En el proyecto se utiliza **Java 11** y **Java 8**._

## Despliegue 📦

_Para desplegar Zuul como proxy escribiremos la dirección junto con el puerto y el nombre de eureka.
```
Dirección de ejemplo:
- http://localhost:8765/second-info-service/api/personas
El puerto corresponde a Zuul 
```

## Construido con 🛠️

_La lista de herramientas utilizadas en el proyecto es la siguiente:_

* [Intellij](https://www.jetbrains.com/help/idea/discover-intellij-idea.html) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [Spring Boot](https://spring.io/projects/spring-boot) - Tecnología basada en Java
* [GitLab](https://gitlab.com/) - Repositorio Git
* [Docker](https://www.docker.com/) - Despliegue de aplicaciones por contenedores

## Versionado 📌

Se usará [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/adrian.imb94/eureka_project/tags).

## Autores ✒️

_Este proyecto ha sido desarrollado por:_

* **Adrián Moral** - [GitLab](https://github.com/adrian.imb94)

## Licencia 📄

Este proyecto no tiene licencia.


