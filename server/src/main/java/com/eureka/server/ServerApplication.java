package com.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Eureka.
 *
 * @author Adrián Moral
 * @version 1
 *
 *  * Testear fácil el funcionamiento:
 *	- Accederemos a http://localhost:8761/.
 *
 */
@SpringBootApplication
@EnableEurekaServer
public class ServerApplication {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ServerApplication.class, args);
	}

}
