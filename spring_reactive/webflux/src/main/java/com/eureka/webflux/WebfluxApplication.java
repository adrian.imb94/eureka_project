package com.eureka.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * WebFlux.
 *
 * @author Adrián Moral
 * @version 1
 *
 * Para poder probar esta aplicación necesitaremos realizar pruebas de carga (loading tests)
 * Usaremos una plataforma que nos lo permita, por ejemplo XAMPP ab:
 *  - ab -n 1600 -c 40 localhost:8080/flux_result
 *
 */
@SpringBootApplication
public class WebfluxApplication {
	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(WebfluxApplication.class, args);
	}
}