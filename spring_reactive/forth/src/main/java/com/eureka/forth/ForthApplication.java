package com.eureka.forth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Forth. (Async)
 *
 * @author Adrián Moral
 * @version 1
 * 
 * Para poder probar esta aplicación necesitaremos realizar pruebas de carga (loading tests)
 * Usaremos una plataforma que nos lo permita, por ejemplo XAMPP ab:
 *  - ab -n 1600 -c 40 localhost:8080/flux_result
 *
 */
@SpringBootApplication
@EnableAsync
public class ForthApplication {
	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ForthApplication.class, args);
	}
}
