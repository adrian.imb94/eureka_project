package com.eureka.third.service;

import com.eureka.third.models.Persona;
import com.eureka.third.repository.PersonaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.List;

/**
 * The type Persona service.
 */
@Service
@RequiredArgsConstructor
public class PersonaService {

    private final PersonaRepository personaRepository;

    /**
     * Update persona.
     *
     * @param persona the persona
     */
    public void updatePersona(Persona persona){
        Persona savedPersona = personaRepository.findById(persona.getId())
                .orElseThrow(() -> new RuntimeException(
                        String.format("No se encontró la persona con el ID ", persona.getId())));

        savedPersona.setId(persona.getId());
        savedPersona.setNombre(persona.getNombre());
        savedPersona.setApellido(persona.getApellido());
        personaRepository.save(persona);
    }

    /**
     * Get all personas list.
     *
     * @return the list
     */
    public List<Persona> getAllPersonas(){
        return personaRepository.findAll();
    }

    /**
     * Delete persona.
     *
     * @param id the id
     */
    public void deletePersona(String id){
        personaRepository.deleteById(id);
    }


    /**
     * Add persona.
     *
     * @param persona the persona
     */
    public void addPersona(Persona persona){
        personaRepository.insert(persona);
    }

    /**
     * Get persona by name persona.
     *
     * @param name the name
     * @return the persona
     */
    public Persona getPersonaByName(String name){
        return personaRepository.findByName(name).orElseThrow(() -> new RuntimeException(
                String.format("No se puede encontrar la persona ", name)));
    }
}
