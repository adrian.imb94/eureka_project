package com.eureka.third;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Second application. (MONGO DB)
 *
 * @author Adrián Moral
 * @version 1
 *
 *  * Urls para testear fácil el funcionamiento:
 *
 *  | - CRUD
 *  | - [GET]http://localhost:8200/api/personas (Devuelve una lista de las personas en la base datos).
 *  | - [POST]http://localhost:8200/api/personas (Añade una persona a la base datos).
 *  | - [PUT]http://localhost:8200/api/personas (Actualiza una persona en la base de datos).
 *  | - [DELETE]http://localhost:8200/api/personas/{id} (Elimina a una persona de la base de datos por ID).
 *  | - [GET]http://localhost:8200/api/personas/{nombre} (Devuelve una persona por nombre).
 *
 *  - http://localhost:8200/{Texto} (Devuelve un String estático).
 *  - http://localhost:8200/second/test (Devuelve un String estático con el número del microservicio).
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SecondApplication {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(SecondApplication.class, args);
	}

	/**
	 * Rest template rest template.
	 *
	 * @return the rest template
	 */
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
