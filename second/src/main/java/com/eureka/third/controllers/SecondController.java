package com.eureka.third.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * The type Second controller.
 */
@RestController
public class SecondController {

    @Autowired
    private RestTemplate restTemplate;

    /**
     * The Web client builder.
     */
    @Autowired
    WebClient.Builder webClientBuilder;

    /**
     * Get test string.
     *
     * @return the string
     */
    @GetMapping("/second/test")
    public String getTest(){
        return "Second microservice.";
    }

    /**
     * Gets second info id.
     *
     * @param id the id
     * @return the second info id
     */
    @RequestMapping("{text}")
    public String getSecondInfoId(@PathVariable("text") String text) {
        return "Texto de ejemplo.";

    }
}
