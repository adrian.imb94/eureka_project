package com.eureka.third.controllers;

import com.eureka.third.models.Persona;
import com.eureka.third.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The type Persona controller.
 */
@RestController
@RequestMapping("/api/personas")
@RequiredArgsConstructor
public class PersonaController {

    @Autowired
    private final PersonaService personaService;

    /**
     * Add persona response entity.
     *
     * @param persona the persona
     * @return the response entity
     */
    @PostMapping
    public ResponseEntity addPersona(@RequestBody Persona persona){
        personaService.addPersona(persona);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * Update persona response entity.
     *
     * @param persona the persona
     * @return the response entity
     */
    @PutMapping
    public ResponseEntity updatePersona(@RequestBody Persona persona){
        personaService.updatePersona(persona);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * Get all personas response entity.
     *
     * @return the response entity
     */
    @GetMapping
    public ResponseEntity<List<Persona>> getAllPersonas(){
        return ResponseEntity.ok(personaService.getAllPersonas());
    }

    /**
     * Get persona by name response entity.
     *
     * @param nombre the nombre
     * @return the response entity
     */
    @GetMapping("/{nombre}")
    public ResponseEntity<Persona> getPersonaByName(@PathVariable String nombre){
        return ResponseEntity.ok(personaService.getPersonaByName(nombre));
    }

    /**
     * Delete persona response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deletePersona(@PathVariable String id){
        personaService.deletePersona(id);
        return ResponseEntity.noContent().build();
    }

}
