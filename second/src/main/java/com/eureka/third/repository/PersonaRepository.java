package com.eureka.third.repository;

import com.eureka.third.models.Persona;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The interface Persona repository.
 */
@Repository
public interface PersonaRepository extends MongoRepository<Persona, String> {

    /**
     * Find by name optional.
     *
     * @param nombre the nombre
     * @return the optional
     */
    @Query("{'nombre': ?0}")
    Optional<Persona> findByName(String nombre);
}
