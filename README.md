# Proyecto Back-end (Adrián Moral)

_Este proyecto trata de emular un proyecto de ejemplo empleando varias de las tecnologías vistas durante la formación._

## Comenzando ⚙️

_Este proyecto utiliza las siguientes tecnologías:_

* **Spring Boot**
* **Zuul** - Será nuestro Gateway
* **Eureka** - Servicio REST de gestión de microservicios
* **Hystrix** - Control de iteraciones entre microservicios
* **Feign** - Proporciona un cliente REST declarativo
* **Config** - Servicio de configuración 
* **MongoDB** - Base de datos noSQL de Mongo
* **Actuator** - Libreria para operaciones de exposición de información/puertos  
* **KeyCloak** - Servicio de manejo de sessiones que usaremos como autentificador
* **Maven** - Manejador de dependencias
* **RabbitMQ** - Sistema de publish/subscribe por colas


_GIT_

La rama **developer** está creada para contener el proyecto, simulando una rama master.

Las ramas de **feature** se utilizarán para implementar código a futuro, serán eliminadas conforme este vaya siendo integrado.


### Pre-requisitos 📋

Necesitaremos trabajar con [Maven](https://maven.apache.org/) como instalador de dependencias.

También será necesario tener Java o algún IDE que nos lo proporcione automáticamente.
En el proyecto se utiliza **Java 11** y **Java 8**.

**Autentificación:**

Para poder utilizar el acceso por autentificación del microservicio **third** tendremos que desplegar un [Keycloak](https://www.keycloak.org/docs/latest/getting_started/) previamente. (El no tenerlo hará que este módulo no tenga funcionalidad, pero no dará errores).

Una vez desplegado tendremos que cambiar la información almacenada en el microservicio **third** modificando el archivo **application.properties**.

**Mongo DB**

Para poder crear la base de datos de MONGO que vamos a utilizar, en el microservicio **second** se encuentra un docker-compose para poder lanzarla en docker.


### Instalación 🔧

_Para poder lanzar todos los módulos del repositorio, será necesario lanzar primero el módulo de eureka llamado "server"._

_También será necesario tener liberados los puertos utilizados por cada uno de los microservicios._


## Despliegue 📦

_Falta de implementar._

## Construido con 🛠️

_La lista de herramientas utilizadas en el proyecto es la siguiente:_

* [Intellij](https://www.jetbrains.com/help/idea/discover-intellij-idea.html) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [Spring Boot](https://spring.io/projects/spring-boot) - Tecnología basada en Java
* [GitLab](https://gitlab.com/) - Repositorio Git
* [Docker](https://www.docker.com/) - Despliegue de aplicaciones por contenedores

## Versionado 📌

Se usará [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/adrian.imb94/eureka_project/tags).

## Autores ✒️

_Este proyecto ha sido desarrollado por:_

* **Adrián Moral** - [GitLab](https://github.com/adrian.imb94)

## Licencia 📄

Este proyecto no tiene licencia.


