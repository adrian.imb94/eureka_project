package com.eureka.hystrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * Hystrix application.
 *
 * @author Adrián Moral
 * @version 1
 * Urls para testear fácil el funcionamiento:
 * - 'http://localhost:8100/actuator' (Actuator del microservicio)
 * - 'http://localhost:3035/hystrix' (Hystrix Dashboard)
 * - Ingresamos en esta página 'http://localhost:8100/actuator/hystrix.stream' y le damos a 'Monitor Stream'.
 * - Por último enviaremos una petición al microservicio (A través de Zuul: 'http://localhost:8765/first-info-service/' o directamente: 'http://localhost:8100/')
 *
 */
@SpringBootApplication
@EnableHystrixDashboard
@EnableDiscoveryClient
public class HystrixApplication {

	/**
	 * Punto de entrada de la aplicación.
	 *
	 * @param args por defecto.
	 */
	public static void main(String[] args) {
		SpringApplication.run(HystrixApplication.class, args);
	}

}
