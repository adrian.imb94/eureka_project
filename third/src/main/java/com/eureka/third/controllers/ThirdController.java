package com.eureka.third.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * The type Third controller.
 */
@RestController
public class ThirdController {

    @Autowired
    private RestTemplate restTemplate;

    /**
     * The Web client builder.
     */
    @Autowired
    WebClient.Builder webClientBuilder;

    /**
     * Get test string.
     *
     * @return the string
     */
    @GetMapping("/third/test")
    public String getTest(){
        return "Third microservice.";
    }


}