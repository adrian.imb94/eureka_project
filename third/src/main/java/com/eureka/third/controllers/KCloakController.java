package com.eureka.third.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * The type K cloak controller.
 */
@RestController
@RequestMapping("api")
public class KCloakController {

    /**
     * Get main string.
     *
     * @param httpServletRequest the http servlet request
     * @return the string
     */
    @GetMapping("/public")
    public String getMain(HttpServletRequest httpServletRequest){
        return "Pública.";
    }

    /**
     * Logout string.
     *
     * @param request the request
     * @return the string
     * @throws ServletException the servlet exception
     */
    @GetMapping("/private")
    public String logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "Privada.";
    }
}
