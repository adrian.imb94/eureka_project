package com.eureka.third;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

/**
 * Third application. (KEYCLOAK)
 *
 * @author Adrián Moral
 * @version 1
 *
 *  * Urls para testear fácil el funcionamiento:
 *
 *	- http://localhost:8300/third/test (Devuelve un String estático con el número del microservicio).
 *
 *	| - KeyCloak
 *  | - http://localhost:8300/api/private (IP privada) {admin/admin}
 *  | - http://localhost:8300/api/public (IP pública)
 *
 *  * En caso de no tener KeyCloak configurado:
 *   - docker run -p 8180:8180 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin quay.io/keycloak/keycloak:11.0.3
 *   - Configuramos KeyCloak siguiendo algún tutorial básico.
 *   - Probando peticiones:
 *   - [POST] http://localhost:8080/auth/realms/{keycloakRealm}/protocol/openid-connect/token (Petición POST a esta dirección)
 *   - Esta petición será enviada junto con un Body KEY/VALUE con (client_id, username, password, grant_type, client_secret)
 *   - Nos devolverá un token de acceso
 *   - Error 'Invalid parameter: redirect_uri':
 *   - Si nos devuelve este error necesitamos configurar 'Valid Redirect URIs' en el cliente de keycloak.
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ThirdApplication {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ThirdApplication.class, args);
	}

	/**
	 * Rest template rest template.
	 *
	 * @return the rest template
	 */
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
