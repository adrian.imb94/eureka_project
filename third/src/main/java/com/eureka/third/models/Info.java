package com.eureka.third.models;

/**
 * The type Info.
 */
public class Info {
    private String id;
    private String name;

    /**
     * Instantiates a new Info.
     *
     * @param id   the id
     * @param name the name
     */
    public Info(String id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }
}