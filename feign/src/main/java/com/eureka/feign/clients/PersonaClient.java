package com.eureka.feign.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The interface Persona client.
 */
@FeignClient("second-info-service")
public interface PersonaClient {

    /**
     * Gets all personas.
     *
     * @return the all personas
     */
    @RequestMapping("/api/personas")
    String getAllPersonas();

    /**
     * Gets test.
     *
     * @return the test
     */
    @RequestMapping("/test")
    String getTest();
}
