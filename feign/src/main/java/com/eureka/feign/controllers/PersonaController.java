package com.eureka.feign.controllers;

import com.eureka.feign.clients.PersonaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Persona controller.
 */
@RestController
public class PersonaController {

    /**
     * The Persona client.
     */
    final PersonaClient personaClient;

    /**
     * Instantiates a new Persona controller.
     *
     * @param personaClient the persona client
     */
    @Autowired
    public PersonaController(PersonaClient personaClient){
        this.personaClient = personaClient;
    }

    /**
     * Get test string.
     *
     * @return the string
     */
    @GetMapping("/test")
    public String getTest(){
        return personaClient.getTest();
    }

    /**
     * Get all personas string.
     *
     * @return the string
     */
    @GetMapping("/api/personas")
    public String getAllPersonas(){
        return personaClient.getAllPersonas();
    }

}
