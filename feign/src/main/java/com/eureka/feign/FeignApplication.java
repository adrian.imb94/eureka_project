package com.eureka.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Feign application. (FEIGN)
 *
 * @author Adrián Moral
 * @version 1
 *
 *	Este microservicio se encarga de reflejar el microservicio Second.
 *
 *  * Testear fácil el funcionamiento:
 *	- http://localhost:8210/api/personas
 *
 *
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class FeignApplication {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(FeignApplication.class, args);
	}

}
